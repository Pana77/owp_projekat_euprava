package com.controllers;

import com.exceptions.UserNotFoundException;
import com.models.Korisnik;
import com.models.Uloga;
import com.models.VestOObolelima;
import com.services.KorisnikService;
import com.services.VestOObolelimaService;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.List;

@Controller
public class VestOObolelimaController {
    @Autowired
    private VestOObolelimaService service;
    @Autowired
    private KorisnikService korisnikService;

    @GetMapping("/infected")
    public String vestOObolelimaList(Model model){
        List<VestOObolelima> list = service.sveVestiOObolelima();
        model.addAttribute("infected", new VestOObolelima());
        model.addAttribute("allInfected", list);
        return "infected";
    }

    @GetMapping("/infected/new")
    public String newInfected(Model model, HttpServletRequest request) throws UserNotFoundException {
        model.addAttribute("infected", new VestOObolelima());
        model.addAttribute("pageTitle", "Add news");
        Cookie[] cookies = request.getCookies();
        if (cookies != null) {
            for (Cookie cookie : cookies) {
                if(cookie.getValue().contains("@")){
                    Korisnik temp = korisnikService.get(cookie.getValue());
                    if(temp.getUloga().equals(Uloga.ADMINISTRATOR)){
                        return "newVestOObolelima";
                    }else{
                        return "redirect:/home";
                    }
                }
            }
        }
        return "redirect:/home";
    }

    @PostMapping("/infected/save")
    public String saveNews(VestOObolelima vestOObolelima, RedirectAttributes redirectAttributes) {
        service.save(vestOObolelima);
        redirectAttributes.addFlashAttribute("message", "Vesti su sacuvane");
        return "redirect:/infected";
    }

    @GetMapping("/infected/edit/{id}")
    public String editInfected(@PathVariable("id") Long id, Model model, RedirectAttributes redirectAttributes){
        VestOObolelima vestOObolelima = service.get(id);
        model.addAttribute("infected", vestOObolelima);
        model.addAttribute("pageTitle",
                "Edit news (name:" + vestOObolelima.getId() + ")");
        return "newInfected";
    }

    @GetMapping("/infected/delete/{id}")
    public String deleteNews(@PathVariable("id") Long id, RedirectAttributes redirectAttributes){
        service.delete(id);
        redirectAttributes.addFlashAttribute("message", "Vesti su obrisane");
        return "redirect:/infected";
    }
}
