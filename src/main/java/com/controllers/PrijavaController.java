package com.controllers;

import com.exceptions.UserNotFoundException;
import com.models.Korisnik;
import com.models.Pacijent;
import com.models.Prijava;
import com.models.Vakcina;
import com.services.KorisnikService;
import com.services.PacijentService;
import com.services.PrijavaService;
import com.services.VakcinaService;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.time.LocalDateTime;
import java.util.List;

@Controller
public class PrijavaController {
    @Autowired
    private PrijavaService service;

    @Autowired
    private KorisnikService korisnikService;

    @Autowired
    private PacijentService pacijentService;

    @Autowired
    private VakcinaService vakcinaService;

    @PostMapping("application/save")
    public String savePrijava(Prijava prijava, HttpServletRequest httpServletRequest, RedirectAttributes redirectAttributes) throws UserNotFoundException {
        Prijava tempPrijava = prijava;
        Korisnik tempKorisnik = new Korisnik();
        Cookie[] cookies = httpServletRequest.getCookies();
        if (cookies != null) {
            for (Cookie cookie : cookies) {
                if(cookie.getValue().contains("@")){
                    tempKorisnik  = korisnikService.get(cookie.getValue());
                }
            }
        }
        List<Pacijent> lista = pacijentService.getAllPacijenti();
        Pacijent pacijent = new Pacijent();
        for(Pacijent pacijentTemp : lista){
            if(pacijentTemp.getPacijentId() == tempKorisnik.getId()){
                pacijent = pacijentTemp;
            }
        }
        Vakcina tempVakcina = vakcinaService.get(tempPrijava.getVakcinaId());

        tempPrijava.setDatumIVreme(LocalDateTime.now());
        tempPrijava.setPacijent(pacijent);
        tempPrijava.setVakcina(tempVakcina);
        service.save(tempPrijava);

        redirectAttributes.addFlashAttribute("message", "Proizvodjac je sacuvan");
        return "redirect:/homepanel";
    }
    @GetMapping("/application/new")
    public String addPrijava(Model model){
        List<Vakcina> vakcine = vakcinaService.listAllQuantity();
        model.addAttribute("vaccines", vakcine);
        model.addAttribute("applicat", new Prijava());
        model.addAttribute("method", "/application/save");
        return "newApplication";
    }

    @GetMapping("/vaccination")
    public String allPrijave(Model model){
        List<Prijava> vakcine = service.listAll();
        model.addAttribute("applicat", new Prijava());
        model.addAttribute("applicats", vakcine);
        model.addAttribute("method", "/application/save");
        return "vaccination";
    }

    @GetMapping("/application/search")
    public String searchVakcine(Model model, @RequestParam(required = false) String query) {
        List<Prijava> prijave = service.searchPrijave(query);
        model.addAttribute("applicats", prijave);
        return "applicationsearchresult";
    }


    @GetMapping("/vaccination/{id}")
    public String vakcinacija(@PathVariable("id") Long id, Model model, RedirectAttributes redirectAttributes) {
        pacijentService.update(pacijentService.getPacijentById(id));
        List<Prijava> prijave = service.listAll();
        for(Prijava prijava : prijave){
            if(prijava.getPacijent().getPacijentId() == id){
                Long temp = prijava.getVakcina().getDostupnaKolicina();
                Vakcina vakcina = prijava.getVakcina();
                vakcina.setDostupnaKolicina(temp-1);
                vakcinaService.save(vakcina);
                service.delete(vakcina.getId());
            }
        }
        return "redirect:/vaccination";
    }
}
