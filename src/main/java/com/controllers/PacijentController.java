package com.controllers;

import com.exceptions.UserNotFoundException;
import com.models.Pacijent;
import com.services.KorisnikService;
import com.services.PacijentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.List;

@Controller
public class PacijentController {
    @Autowired
    private PacijentService service;
    @Autowired
    private KorisnikService korisnikService;

    @GetMapping("/patient")
    public String sviPacijenti(Model model) throws UserNotFoundException{
        List<Pacijent> lista = service.getAllPacijenti();
        model.addAttribute("allPatients", lista);
        return "patient";
    }

    @PostMapping("/patient/update")
    public String update(Pacijent pacijent, RedirectAttributes redirectAttributes){
        service.update(pacijent);
        redirectAttributes.addFlashAttribute("message", "Pacijent je azuriran");
        return "redirect:/patient";
    }

    @GetMapping("/patient/edit/{id}")
    public String editKorisnik(@PathVariable("id") Long id, Model model, RedirectAttributes redirectAttributes){
        Pacijent pacijent = service.getPacijentById(id);
        model.addAttribute("patient", pacijent);
        model.addAttribute("pageTitle", "Edit patient(name:" + pacijent.getKorisnik().getIme() + ")");
        return "newPatient";
    }

    @GetMapping("/patient/delete/{id}")
    public String delete(@PathVariable("id") Long id, RedirectAttributes redirectAttributes){
        service.delete(id);
        korisnikService.delete(id);
        redirectAttributes.addFlashAttribute("message", "Pacijent je obrisan");
        return "redirect:/patient";
    }

}
