package com.controllers;

import com.exceptions.UserNotFoundException;
import com.models.Korisnik;
import com.models.ProizvodjacVakcine;
import com.models.Uloga;
import com.services.KorisnikService;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@Controller
public class LoginController {
    public static final String USER_KEY = "KORISNIK";

    private String url;
    @Autowired
    private KorisnikService service;

    @GetMapping("/loginn")  //*********************
    public String loginStranica(Model model){
        model.addAttribute("manufacturer", new ProizvodjacVakcine());
        model.addAttribute("pageTitle", "Dodaj novog proizvodjaca");
        return "login";
    }

    @RequestMapping("/")
    public String homeStranica2(){ return "redirect:/home"; }   //*******************

    @GetMapping("/index")
    public String showPanel(Model model, HttpServletRequest httpServletRequest) throws UserNotFoundException{
        Korisnik temp = new Korisnik();
        Cookie[] cookies = httpServletRequest.getCookies();
        if (cookies != null) {
            for (Cookie cookie : cookies) {
                if (cookie.getValue().contains("@")) {
                    temp = service.get(cookie.getValue());
                    break;
                }else{

                }
            }
            if(temp.getUloga() == null){
                return "redirect:/loginn";
            }
            if(temp.getUloga() == Uloga.ADMINISTRATOR){
                model.addAttribute("adminstyle","visible");
                model.addAttribute("patientestyle","hidden");
                model.addAttribute("employeestyle","hidden");
            } else if (temp.getUloga() == Uloga.MEDICINSKO_OSOBLJE) {
                model.addAttribute("employeestyle","visible");
                model.addAttribute("adminstyle","hidden");
                model.addAttribute("patientestyle","hidden");
            }else{
                model.addAttribute("patientstyle","visible");
                model.addAttribute("adminstyle","hidden");
                model.addAttribute("employeestyle","hidden");

            }
        }
        return "mainloggedpanel";
    }

    @PostMapping("/loginn/save")
    public String Login(@RequestParam("email") String email, @RequestParam("lozinka") String lozinka, HttpServletRequest httpServletRequest, @CookieValue(value = "sessionID", defaultValue = "") String sessionID, HttpServletResponse httpServletResponse, Model model) throws UserNotFoundException{

        if (service.get(email, lozinka) != null) {
            Korisnik temp = service.get(email, lozinka);
            Boolean ulogovani = false;
            Cookie[] cookies = httpServletRequest.getCookies();
            if (cookies != null) {
                for (Cookie cookie : cookies) {
                    if (cookie.getValue().contains(temp.getEmail())) {
                        ulogovani = true;
                        break;
                    }else{
                        ulogovani = false;
                    }
                }
            }
            if (ulogovani) {
                model.addAttribute("error", "Korisnik je već ulogovan.");
                setSessionCookie(httpServletResponse, temp.getEmail());
                return "redirect:/index";
            }if (sessionID.isEmpty()) {
                sessionID = generateSessionID();
                setSessionCookie(httpServletResponse, temp.getEmail());
            }
            setLoggedInUser(sessionID, temp.getEmail());
            return "redirect:/index";
        } else {
            model.addAttribute("error", "Pogresan mail ili lozinka");
            return "index";
        }

    }
    private void setSessionCookie(HttpServletResponse response, String sessionID) {
        Cookie sessionCookie = new Cookie("sessionID", sessionID);
        sessionCookie.setMaxAge(3600);
        sessionCookie.setPath("/");
        response.addCookie(sessionCookie);
    }

    private String generateSessionID() {
        return UUID.randomUUID().toString();
    }

    private Map<String, String> ulogovaniKorisnici = new HashMap<>();

    private void setLoggedInUser(String sessionID, String email) {
        ulogovaniKorisnici.put(sessionID, email);
    }

}
