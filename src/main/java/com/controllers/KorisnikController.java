package com.controllers;

import com.exceptions.UserNotFoundException;
import com.models.Korisnik;
import com.models.Pacijent;
import com.models.Prijava;
import com.models.Uloga;
import com.services.KorisnikService;
import com.services.PacijentService;
import com.services.PrijavaService;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.time.LocalDateTime;
import java.util.List;

@Controller
public class KorisnikController {

    @Autowired
    private KorisnikService service;
    @Autowired
    private PrijavaService prijavaService;
    @Autowired
    private PacijentService pacijentService;

    @GetMapping("/user")
    public String sviKorisnici(Model model){
        List<Korisnik> listKorisnike = service.listAll();
        model.addAttribute("allUsers", listKorisnike);
        return "user";
    }

    @GetMapping("/user/new")
    public String addKorisnik(Model model){
        Pacijent temp = new Pacijent();
        temp.setPrimio(0);
        model.addAttribute("patient", temp);
        model.addAttribute("user", new Korisnik());
        model.addAttribute("method", "/user/save");
        model.addAttribute("pageTitle", "Dodaj novog pacijenta");
        return "newUser";
    }

    @PostMapping("/user/save")
    public String save(Korisnik korisnik, RedirectAttributes redirectAttributes,HttpServletRequest httpServletRequest) throws UserNotFoundException {
        Korisnik temp1;
        if(service.get(korisnik.getEmail()) != null){
            Cookie[] cookies = httpServletRequest.getCookies();
            if (cookies != null) {
                for (Cookie cookie : cookies) {
                    if(cookie.getValue().contains("@")){
                        temp1 = service.get(cookie.getValue());
                        korisnik.setUloga(temp1.getUloga());
                    }
                }
            }
        }
        if(korisnik.getUloga() == null){
            korisnik.setUloga(Uloga.PACIJENT);
        }
        LocalDateTime datumIVreme = LocalDateTime.now();
        korisnik.setDatumIVremeRegistracije(datumIVreme);
        service.save(korisnik);
        Korisnik tempKorisnik = service.get(korisnik.getEmail());
        if(korisnik.getUloga().equals(Uloga.PACIJENT)){
            Pacijent temp = new Pacijent();
            temp.setPacijentId(tempKorisnik.getId());
            pacijentService.save(temp);
        }
        redirectAttributes.addFlashAttribute("message", "Korisnik je sacuvan");
        return "redirect:/index";
    }

    @PostMapping ("/user/update")
    public String update(Korisnik korisnik, RedirectAttributes redirectAttributes) throws UserNotFoundException {
        korisnik.setAdresa("adresicaaa");
        service.save(korisnik);
        redirectAttributes.addFlashAttribute("message", "Korisnik je azuriran");
        return "redirect:/user";
    }

    @GetMapping("/user/logout")
    public String logout(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) {
        // Get the session ID cookie
        Cookie[] cookies = httpServletRequest.getCookies();
        if (cookies != null) {
            for (Cookie cookie : cookies) {
                if (cookie.getName().equals("sessionID")) {
                    // Remove the session ID cookie
                    cookie.setValue("");
                    cookie.setMaxAge(0);
                    cookie.setPath("/");
                    httpServletResponse.addCookie(cookie);
                    break;
                }
            }
        }
        // Redirect to the login page
        return "redirect:/loginlogin";
    }
    @GetMapping("/user/edit")
    public String editKorisnikWithCookie(HttpServletRequest httpServletRequest, Model model, RedirectAttributes redirectAttributes){
        try{
            Korisnik temp = new Korisnik();
            Cookie[] cookies = httpServletRequest.getCookies();
            if (cookies != null) {
                for (Cookie cookie : cookies) {
                    if(cookie.getValue().contains("@")){
                        temp = service.get(cookie.getValue());
                    }
                }
            }
            List<Prijava> tempPrijava = prijavaService.listPrijaveByKorisnikId(temp.getId());
            if(temp.getUloga() == Uloga.PACIJENT){
                Pacijent pacijent = pacijentService.getPacijentById(temp.getId());
                model.addAttribute("patient", pacijent);
            }else{
                Pacijent pacijent = new Pacijent();
                pacijent.setPrimio(0);
                model.addAttribute("patient", pacijent);
            }


            model.addAttribute("applicat", new Prijava());
            model.addAttribute("applications", tempPrijava);
            model.addAttribute("user", temp);
            model.addAttribute("method", "/user/update");
            return "newUser";

        } catch (UserNotFoundException e) {
            redirectAttributes.addFlashAttribute("message", "Korisnik ne moze biti izmenjen");
            return "redirect:/index";
        }
    }
    @GetMapping("/user/edit/{id}")
    public String editKorisnikWithoutCookie(@PathVariable("id") Long id, Model model, RedirectAttributes redirectAttributes){
        try{
            Korisnik korisnik = service.get(id);
            model.addAttribute("user", korisnik);
            model.addAttribute("method", "/user/update");
            model.addAttribute("pageTitle", "Promeni korisnika (Email:" + korisnik.getEmail() + ")");
            return "newUser";

        } catch (UserNotFoundException e) {
            redirectAttributes.addFlashAttribute("message", "Korisnik ne moze biti izmenjen");
            return "redirect:/index";
        }
    }

    @GetMapping("application/delete/{id}")
    public String deletePrijava(@PathVariable("id") Long id, RedirectAttributes redirectAttributes){
        prijavaService.delete(id);
        redirectAttributes.addFlashAttribute("message", "Obrisan");
        return "redirect:/user/edit";

    }
    @GetMapping("user/delete/{id}")
    public String deleteKorisnik(@PathVariable("id") Long id, RedirectAttributes redirectAttributes) throws UserNotFoundException {
        Korisnik korisnik = service.get(id);
        if(korisnik.getUloga().equals(Uloga.PACIJENT)){
            pacijentService.delete(id);
        }
        service.delete(id);
        redirectAttributes.addFlashAttribute("message", "Korisnik je izrisan");
        return "redirect:/user";

    }

}