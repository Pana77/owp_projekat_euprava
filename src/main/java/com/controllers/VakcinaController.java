package com.controllers;

import com.exceptions.UserNotFoundException;
import com.models.Korisnik;
import com.models.ProizvodjacVakcine;
import com.models.Uloga;
import com.models.Vakcina;
import com.services.KorisnikService;
import com.services.ProizvodjacVakcineService;
import com.services.VakcinaService;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.List;
@Controller
public class VakcinaController {
    @Autowired
    private VakcinaService service;

    @Autowired
    private ProizvodjacVakcineService proizvodjacVakcineService;
    @Autowired
    private KorisnikService korisnikService;

    @GetMapping("/vaccine")
    public String sveVakcine(Model model){
        List<Vakcina> list = service.listAll();
        model.addAttribute("allVaccines", list);
        return "vaccine";
    }

    @GetMapping("/vaccinetabela")
    public String VakcineTable(Model model, HttpServletRequest httpServletRequest, @RequestParam(name="searchBy", required = false) String searchBy, @RequestParam(name = "orderBy", required = false) String orderBy) throws UserNotFoundException {
        if(searchBy == null || orderBy == null){
            searchBy = "id";
            orderBy = "asc";
        }
        List<Vakcina> list = service.getAllVakcineSorted(searchBy,orderBy);
        model.addAttribute("allVaccines", list);
        Cookie[] cookies = httpServletRequest.getCookies();
        if (cookies != null) {
            for (Cookie cookie : cookies) {
                if(cookie.getValue().contains("@")){
                    Korisnik temp = korisnikService.get(cookie.getValue());
                    if(!temp.getUloga().equals(Uloga.valueOf("PACIJENT"))){
                        return "vaccinestabela";
                    }else{
                        return "redirect:/home";
                    }
                }
            }
        }
        return "redirect:/home";
    }

    @GetMapping("/vaccine/nova")
    public String addVacine(Model model){
        List<ProizvodjacVakcine> proizvodjaciVakcina = proizvodjacVakcineService.listAll();

        model.addAttribute("vaccine", new Vakcina());
        model.addAttribute("manufacturers", proizvodjaciVakcina);
        return "newVaccine";
    }

    @GetMapping("/vaccine/search")
    public String searchVaccines(Model model, @RequestParam(required = false) String query) {
        List<Vakcina> vakcine = service.searchVakcine(query);

        model.addAttribute("vaccines", vakcine);
        return "vaccinesearchresult";
    }


    @PostMapping("/vaccine/save")
    public String save(Vakcina vakcina, RedirectAttributes redirectAttributes) {

        ProizvodjacVakcine temp = proizvodjacVakcineService.get(vakcina.getProizvodjacId());
        vakcina.setProizvodjacVakcine(temp);
        service.save(vakcina);
        redirectAttributes.addFlashAttribute("message", "Vakcina je sacuvana");
        return "redirect:/vaccinetabela";
    }

    @GetMapping("/vaccine/edit/{id}")
    public String editVakcina(@PathVariable("id") Long id, Model model, HttpServletRequest httpServletRequest, RedirectAttributes redirectAttributes) throws UserNotFoundException {
        List<ProizvodjacVakcine> proizvodjaciVakcina = proizvodjacVakcineService.listAll();
        Vakcina vakcina = service.get(id);
        Cookie[] cookies = httpServletRequest.getCookies();
        if (cookies != null) {
            for (Cookie cookie : cookies) {
                if(cookie.getValue().contains("@")){
                    Korisnik temp = korisnikService.get(cookie.getValue());
                    if(temp.getUloga().equals(Uloga.valueOf("ADMINISTRATOR"))){
                        model.addAttribute("dissic", "readonly");  //**********
                        model.addAttribute("vaccine", vakcina);
                        model.addAttribute("manufacturers", proizvodjaciVakcina);
                        model.addAttribute("manufacturer", vakcina.getProizvodjacVakcine());
                        return "newVaccine";
                    }else if(temp.getUloga().equals(Uloga.valueOf("MEDICINSKO_OSOBLJE"))){
                        return "redirect:/home";
                    }
                }
            }
        }
        return "redirect:/home";
    }

    @GetMapping("/vaccine/info/{id}")
    public String infoVakcina(@PathVariable("id") Long id, Model model, RedirectAttributes redirectAttributes){
        List<ProizvodjacVakcine> proizvodjaciVakcina = proizvodjacVakcineService.listAll();
        Vakcina vakcina = service.get(id);
        model.addAttribute("dissic", "readonly"); //*************
        model.addAttribute("vaccine", vakcina);
        model.addAttribute("manufacturers", proizvodjaciVakcina);
        model.addAttribute("manufacturer", vakcina.getProizvodjacVakcine());
        return "onlyVakcina";
    }

    @GetMapping("/vaccine/delete/{id}")
    public String deleteVakcina(@PathVariable("id") Long id, RedirectAttributes redirectAttributes){
        service.delete(id);
        redirectAttributes.addFlashAttribute("message", "Vakcina je izbrisana");
        return "redirect:/vaccine";
    }
}