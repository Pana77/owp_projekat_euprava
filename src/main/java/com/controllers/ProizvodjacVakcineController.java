package com.controllers;

import com.models.ProizvodjacVakcine;
import com.services.ProizvodjacVakcineService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.List;
@Controller
public class ProizvodjacVakcineController {
    @Autowired
    private ProizvodjacVakcineService service;

    @GetMapping("/manufacturer")
    public String sviProizvodjaciVakcine(Model model){
        List<ProizvodjacVakcine> list = service.listAll();
        model.addAttribute("allManufacturers", list);
        return "manufacturer";
    }
    @GetMapping("/manufacturer/new")
    public String newManufacturer(Model model){
        model.addAttribute("manufacturer", new ProizvodjacVakcine());
        model.addAttribute("pageTitle", "Dodaj novog proizvodjaca");
        return "newManufacturer";
    }
    @PostMapping("/manufacturer/save")
    public String saveProizvodjacVakcine(ProizvodjacVakcine proizvodjacVakcine, RedirectAttributes redirectAttributes) {
        service.save(proizvodjacVakcine);
        redirectAttributes.addFlashAttribute("message", "Proizvodjac je sacuvan");
        return "redirect:/manufacturer";
    }
    @GetMapping("/manufacturer/edit/{id}")
    public String editProizvodjacVakcine(@PathVariable("id") Long id, Model model, RedirectAttributes redirectAttributes){
        ProizvodjacVakcine proizvodjacVakcine = service.get(id);
        model.addAttribute("manufacturer", proizvodjacVakcine);
        model.addAttribute("pageTitle",
                "Izmeni proizvodjaca (ime:"+ proizvodjacVakcine.getIme() +", drzava:" + proizvodjacVakcine.getDrzavaProizvodnje() + ")");
        return "newManufacturer";
    }
    @GetMapping("/manufacturer/delete/{id}")
    public String deleteProizvodjacVakcine(@PathVariable("id") Long id, RedirectAttributes redirectAttributes){
        service.delete(id);
        redirectAttributes.addFlashAttribute("message", "Proizvodjac je izbrisan");
        return "redirect:/manufacturer";
    }
    @GetMapping("/error")
    public String handleError() {
        return "error";
    }
}