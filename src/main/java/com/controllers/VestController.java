package com.controllers;

import com.exceptions.UserNotFoundException;
import com.models.Korisnik;
import com.models.Uloga;
import com.models.Vest;
import com.models.VestOObolelima;
import com.services.KorisnikService;
import com.services.VestOObolelimaService;
import com.services.VestService;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.time.LocalDate;
import java.util.List;

@Controller
public class VestController {
    @Autowired
    private VestService service;
    @Autowired
    private VestOObolelimaService vestOObolelimaService;
    @Autowired
    private KorisnikService korisnikService;

    @GetMapping("/news")
    public String showVestList(Model model){
        List<Vest> list = service.sveVesti();
        model.addAttribute("news", new Vest());
        model.addAttribute("allNews", list);
        return "news";
    }
    @GetMapping("/home")
    public String vestHome(Model model){
        List<Vest> list = service.sveVesti();
        model.addAttribute("allNews", list);
        List<VestOObolelima> vestOObolelima = vestOObolelimaService.sveVestiOObolelima();
        if(vestOObolelima != null) {
            for (VestOObolelima oboleli : vestOObolelima) {
                LocalDate danas = LocalDate.now();
                if (oboleli.getDatumObjavljivanja().isEqual(danas)) {
                    model.addAttribute("brojobolelih", oboleli.getBrojObolelih());
                    model.addAttribute("brojhospitalizovanih", oboleli.getBrojHospitalizovanih());
                    model.addAttribute("brojtestiranih", oboleli.getBrojTestiranih());
                    model.addAttribute("brojpacijenatanarespiratorima", oboleli.getBrojPacijenataNaRespiratorima());
                    model.addAttribute("brojobolelihodpocetka", oboleli.getBrojObolelihOdPocetka());
                }
            }
        }

        model.addAttribute("news", new Vest());
        return "newsHome";
    }

    @GetMapping("/news/new")
    public String newForm(Model model, HttpServletRequest httpServletRequest) throws UserNotFoundException {
        model.addAttribute("news", new Vest());
        Cookie[] cookies = httpServletRequest.getCookies();
        if (cookies != null) {
            for (Cookie cookie : cookies) {
                if(cookie.getValue().contains("@")){
                    Korisnik temp = korisnikService.get(cookie.getValue());
                    if(temp.getUloga().equals(Uloga.valueOf("ADMINISTRATOR"))){
                        return "newNews";
                    }else{
                        return "redirect:/home";
                    }
                }
            }
        }
        return "redirect:/home";
    }

    @PostMapping("/news/save")
    public String saveVest(Vest vest, RedirectAttributes redirectAttributes) {
        service.save(vest);
        redirectAttributes.addFlashAttribute("message", "Vest je sacuvana");
        return "redirect:/news";
    }

    @GetMapping("/news/edit/{id}")
    public String editVest(@PathVariable("id") Long id, Model model, RedirectAttributes redirectAttributes){
        Vest vest = service.get(id);
        model.addAttribute("news", vest);
        redirectAttributes.addFlashAttribute("message","Vest je azurirana");
        return "newNews";
    }

    @GetMapping("/news/delete/{id}")
    public String deleteVest(@PathVariable("id") Long id, RedirectAttributes redirectAttributes){
        service.delete(id);
        redirectAttributes.addFlashAttribute("message", "Vest je obrisana");
        return "redirect:/news";
    }
}
