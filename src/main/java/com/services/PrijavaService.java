package com.services;

import com.models.Prijava;
import com.repositories.PrijavaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class PrijavaService {
    @Autowired
    private PrijavaRepository repo;
    public List<Prijava> listAll(){
        return (List<Prijava>) repo.findAllPrijave();
    }

    public List<Prijava> listAllByPacijent(String searchTerm){
        return (List<Prijava>) repo.findAllPrijaveByKorisnik(searchTerm);
    }

    public void save(Prijava prijava) {
        if(repo.findPrijavaById(prijava.getId()) != null){
            repo.update(prijava);
        }   else{
            repo.save(prijava);
        }
    }

    public List<Prijava> searchPrijave(String query) {
        return repo.searchPrijave(query);
    }
    public List<Prijava> listPrijaveByKorisnikId(Long id){
        return (List<Prijava>) repo.findPrijavaById(id);
    }

    public Prijava get(Long id) {
        Prijava result = repo.findPrijavaById(id);
        return result;
    }

    public void delete(Long id) {
        repo.delete(id);
    }
}
