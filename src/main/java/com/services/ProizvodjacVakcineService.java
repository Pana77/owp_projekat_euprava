package com.services;

import com.dao.ProizvodjacVakcineInterface;
import com.models.ProizvodjacVakcine;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProizvodjacVakcineService {
    @Autowired
    private ProizvodjacVakcineInterface repo;
    public List<ProizvodjacVakcine> listAll(){
        return(List<ProizvodjacVakcine>) repo.findAllProizvodjaceVakcina();
    }

    public void save(ProizvodjacVakcine proizvodjacVakcine){
        if(repo.findProizvodjacaVakcinaById(proizvodjacVakcine.getId()) != null){
            repo.update(proizvodjacVakcine);
        }else{
            repo.save(proizvodjacVakcine);
        }
    }

    public ProizvodjacVakcine get(Long id){
        ProizvodjacVakcine proizvodjacVakcine = repo.findProizvodjacaVakcinaById(id);
        return proizvodjacVakcine;
    }

    public void delete(Long id){ repo.delete(id); }
}
