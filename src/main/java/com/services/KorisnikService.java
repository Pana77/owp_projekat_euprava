package com.services;

import com.exceptions.UserNotFoundException;
import com.models.Korisnik;
import com.repositories.KorisnikRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class KorisnikService {
    @Autowired
    private KorisnikRepository repo;

    public List<Korisnik> listAll(){ return (List<Korisnik>) repo.findAllKorisnici(); }

    public Korisnik get(Long id) throws UserNotFoundException {
        Korisnik trazeniKorisnikId = repo.findKorisnikById(id);
        return trazeniKorisnikId;
    }
    public Korisnik get(String email) throws UserNotFoundException{
        Korisnik trazeniKorisnikEmail = repo.findKorisnikByEmail(email);
        return trazeniKorisnikEmail;
    }
    public Korisnik get(String email, String lozinka) throws  UserNotFoundException {
        Korisnik trazeniKorisnikCreds = repo.findKorisnikWithCredentials(email, lozinka);
        return trazeniKorisnikCreds;
    }
    public void save(Korisnik korisnik){
        if(repo.findKorisnikById(korisnik.getId()) != null){
            if(korisnik.getLozinka().isEmpty() || korisnik.getLozinka() == null) {
                Korisnik temp = repo.findKorisnikByEmail(korisnik.getEmail());
                korisnik.setLozinka(temp.getLozinka());
            }
            repo.update(korisnik);
        }else{
            repo.save(korisnik);
        }
    }

    public void delete(Long id) {
        repo.delete(id);
    }

}
