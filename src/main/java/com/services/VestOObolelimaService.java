package com.services;

import com.models.VestOObolelima;
import com.repositories.VestOObolelimaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@Service
public class VestOObolelimaService {
    @Autowired
    private VestOObolelimaRepository repo;

    public List<VestOObolelima> sveVestiOObolelima() { return(List<VestOObolelima>) repo.findAllVestiOObolelima(); }

    public void save(VestOObolelima vestOObolelima) {
        Integer dnevnaBrojka = 0;
        if(repo.findVestOObolelimaById(vestOObolelima.getId()) != null){
            repo.update(vestOObolelima);
        } else{
            List<VestOObolelima> sveVestiOObolelima = repo.findAllVestiOObolelima();
            for(VestOObolelima temp : sveVestiOObolelima){
                if (temp.getDatumObjavljivanja().isEqual(LocalDate.now())){
                    dnevnaBrojka++;
                }
            }
            if(dnevnaBrojka == 0){
                repo.save(vestOObolelima);
            }
        }
    }
    public VestOObolelima get(Long id) {
        VestOObolelima vestOObolelima = repo.findVestOObolelimaById(id);
        return vestOObolelima;
    }

    public void delete(Long id) { repo.delete(id); }
}

