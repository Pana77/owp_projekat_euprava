package com.services;

import com.dao.PacijentInterface;
import com.models.Pacijent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PacijentService {
    @Autowired
    private PacijentInterface repo;

    public List<Pacijent> getAllPacijenti() {
        return repo.findAllPacijenti(); // Fixed
    }

    public Pacijent getPacijentById(Long id) {
        return repo.findPacijentById(id); // Fixed
    }

    public void save(Pacijent pacijent) {
        repo.save(pacijent);
    }

    public void update(Pacijent pacijent) {
        repo.update(pacijent);
    }

    public void delete(Long id) {
        repo.delete(id);
    }
}
