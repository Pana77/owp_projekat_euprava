package com.services;


import com.models.Vakcina;
import com.repositories.VakcinaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class VakcinaService {
    @Autowired
    private VakcinaRepository repo;

    public List<Vakcina> listAll() { return(List<Vakcina>) repo.findAllVakcine();}

    public List<Vakcina> listAllQuantity() {
        return(List<Vakcina>) repo.findAllVakcineQuantity();
    }
    public void save(Vakcina vakcina) {
        if(repo.findVakcinaById(vakcina.getId()) != null){
            repo.update(vakcina);
        } else{
            repo.save(vakcina);
        }
    }
    public List<Vakcina> getAllVakcineSorted(String sort, String direction) {
        return repo.findSortedVakcine(sort, direction);
    }
    public List<Vakcina> searchVakcine(String query) {
        return repo.searchVakcine(query);
    }

    public Vakcina get(Long id) {
        Vakcina vakcina = repo.findVakcinaById(id);
        return vakcina;
    }

    public void delete(Long id) { repo.delete(id); }

}