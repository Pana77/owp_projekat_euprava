package com.services;

import com.models.Vest;
import com.repositories.VestRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class VestService {
    @Autowired
    private VestRepository repo;

    public List<Vest> sveVesti() { return(List<Vest>) repo.findAllVesti(); }

    public void save(Vest vest) {
        if(repo.findVestById(vest.getId()) != null){
            repo.update(vest);
        } else{
            repo.save(vest);
        }
    }

    public Vest get(Long id) {
        Vest vest = repo.findVestById(id);
        return vest;
    }

    public void delete(Long id) { repo.delete(id); }
}
