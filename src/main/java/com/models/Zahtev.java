package com.models;

import java.time.LocalDate;
public class Zahtev {

    private Long id;
    private int kolicina;
    private String razlog;
    private LocalDate datum;
    private String stanje;
    private Korisnik radnik;
    private Vakcina vakcina;
    private String komentar;

    public Zahtev(Long id, int kolicina, String razlog, LocalDate datum, String stanje, Korisnik radnik, Vakcina vakcina, String komentar) {
        this.id = id;
        this.kolicina = kolicina;
        this.razlog = razlog;
        this.datum = datum;
        this.stanje = stanje;
        this.radnik = radnik;
        this.vakcina = vakcina;
        this.komentar = komentar;
    }

    public Zahtev() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getKolicina() {
        return kolicina;
    }

    public void setKolicina(int kolicina) {
        this.kolicina = kolicina;
    }

    public String getRazlog() {
        return razlog;
    }

    public void setRazlog(String razlog) {
        this.razlog = razlog;
    }

    public LocalDate getDatum() {
        return datum;
    }

    public void setDatum(LocalDate datum) {
        this.datum = datum;
    }

    public String getStanje() {
        return stanje;
    }

    public void setStanje(String stanje) {
        this.stanje = stanje;
    }

    public Korisnik getRadnik() {
        return radnik;
    }

    public void setRadnik(Korisnik radnik) {
        this.radnik = radnik;
    }

    public Vakcina getVakcina() {
        return vakcina;
    }

    public void setVakcina(Vakcina vakcina) {
        this.vakcina = vakcina;
    }

    public String getKomentar() {
        return komentar;
    }

    public void setKomentar(String komentar) {
        this.komentar = komentar;
    }

    @Override
    public String toString() {
        return "Zahtev{" +
                "id=" + id +
                ", kolicina=" + kolicina +
                ", razlog='" + razlog + '\'' +
                ", datum=" + datum +
                ", stanje='" + stanje + '\'' +
                ", radnik=" + radnik +
                ", vakcina=" + vakcina +
                ", komentar='" + komentar + '\'' +
                '}';
    }
}
