package com.models;

import java.time.LocalDate;
import java.time.LocalDateTime;

public class VestOObolelima {
    private Long id;
    private Long brojObolelih;
    private Long brojTestiranih;
    private Long brojObolelihOdPocetka;
    private Long brojHospitalizovanih;
    private Long brojPacijenataNaRespiratorima;
    private LocalDate datumObjavljivanja;

    public VestOObolelima() {
    }

    public VestOObolelima(Long id, Long brojObolelih, Long brojTestiranih, Long brojObolelihOdPocetka,
                          Long brojHospitalizovanih, Long brojPacijenataNaRespiratorima, LocalDate datumObjavljivanja) {
        this.id = id;
        this.brojObolelih = brojObolelih;
        this.brojTestiranih = brojTestiranih;
        this.brojObolelihOdPocetka = brojObolelihOdPocetka;
        this.brojHospitalizovanih = brojHospitalizovanih;
        this.brojPacijenataNaRespiratorima = brojPacijenataNaRespiratorima;
        this.datumObjavljivanja = datumObjavljivanja;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getBrojObolelih() {
        return brojObolelih;
    }

    public void setBrojObolelih(Long brojObolelih) {
        this.brojObolelih = brojObolelih;
    }

    public Long getBrojTestiranih() {
        return brojTestiranih;
    }

    public void setBrojTestiranih(Long brojTestiranih) {
        this.brojTestiranih = brojTestiranih;
    }

    public Long getBrojObolelihOdPocetka() {
        return brojObolelihOdPocetka;
    }

    public void setBrojObolelihOdPocetka(Long brojObolelihOdPocetka) {
        this.brojObolelihOdPocetka = brojObolelihOdPocetka;
    }

    public Long getBrojHospitalizovanih() {
        return brojHospitalizovanih;
    }

    public void setBrojHospitalizovanih(Long brojHospitalizovanih) {
        this.brojHospitalizovanih = brojHospitalizovanih;
    }

    public Long getBrojPacijenataNaRespiratorima() {
        return brojPacijenataNaRespiratorima;
    }

    public void setBrojPacijenataNaRespiratorima(Long brojPacijenataNaRespiratorima) {
        this.brojPacijenataNaRespiratorima = brojPacijenataNaRespiratorima;
    }

    public LocalDate getDatumObjavljivanja() {
        return datumObjavljivanja;
    }

    public void setDatumObjavljivanja(LocalDate datumObjavljivanja) {
        this.datumObjavljivanja = datumObjavljivanja;
    }

    @Override
    public String toString() {
        return "VestOObolelima{" +
                "id=" + id +
                ", brojObolelih=" + brojObolelih +
                ", brojTestiranih=" + brojTestiranih +
                ", brojObolelihOdPocetka=" + brojObolelihOdPocetka +
                ", brojHospitalizovanih=" + brojHospitalizovanih +
                ", brojPacijenataNaRespiratorima=" + brojPacijenataNaRespiratorima +
                ", datumObjavljivanja=" + datumObjavljivanja +
                '}';
    }
}
