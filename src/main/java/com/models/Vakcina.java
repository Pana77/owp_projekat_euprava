package com.models;

public class Vakcina {

    private Long id;
    private String ime;
    private Long dostupnaKolicina;
    private ProizvodjacVakcine proizvodjacVakcine;
    private Long proizvodjacId;

    public Vakcina() {
    }

    public Vakcina(Long id, String ime, Long dostupnaKolicina, ProizvodjacVakcine proizvodjacVakcine, Long proizvodjacId) {
        this.id = id;
        this.ime = ime;
        this.dostupnaKolicina = dostupnaKolicina;
        this.proizvodjacVakcine = proizvodjacVakcine;
        this.proizvodjacId = proizvodjacId;
    }

    public ProizvodjacVakcine getProizvodjacVakcine() {
        return proizvodjacVakcine;
    }

    public void setProizvodjacVakcine(ProizvodjacVakcine proizvodjacVakcine) {
        this.proizvodjacVakcine = proizvodjacVakcine;
    }

    public Long getProizvodjacId() {
        return proizvodjacId;
    }

    public void setProizvodjacId(Long proizvodjacId) {
        this.proizvodjacId = proizvodjacId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIme() {
        return ime;
    }

    public void setIme(String ime) {
        this.ime = ime;
    }

    public Long getDostupnaKolicina() {
        return dostupnaKolicina;
    }

    public void setDostupnaKolicina(Long dostupnaKolicina) {
        this.dostupnaKolicina = dostupnaKolicina;
    }

    @Override
    public String toString() {
        return "Vakcina{" +
                "id=" + id +
                ", ime='" + ime + '\'' +
                ", dostupnaKolicina=" + dostupnaKolicina +
                ", proizvodjacVakcine=" + proizvodjacVakcine +
                ", proizvodjacId=" + proizvodjacId +
                '}';
    }
}
