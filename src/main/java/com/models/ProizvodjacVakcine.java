package com.models;

public class ProizvodjacVakcine {
    private Long id;
    private String ime;
    private String drzavaProizvodnje;

    public ProizvodjacVakcine() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIme() {
        return ime;
    }

    public void setIme(String ime) {
        this.ime = ime;
    }

    public String getDrzavaProizvodnje() {
        return drzavaProizvodnje;
    }

    public void setDrzavaProizvodnje(String drzavaProizvodnje) {
        this.drzavaProizvodnje = drzavaProizvodnje;
    }

    public ProizvodjacVakcine(Long id, String ime, String drzavaProizvodnje) {
        this.id = id;
        this.ime = ime;
        this.drzavaProizvodnje = drzavaProizvodnje;
    }

    @Override
    public String toString() {
        return "ProizvodjacVakcine{" +
                "id=" + id +
                ", ime='" + ime + '\'' +
                ", drzavaProizvodnje='" + drzavaProizvodnje + '\'' +
                '}';
    }
}
