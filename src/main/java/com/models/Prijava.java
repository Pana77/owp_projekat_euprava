package com.models;

import java.time.LocalDateTime;

public class Prijava {
    private Long id;
    private LocalDateTime datumIVreme;
    private Vakcina vakcina;
    private Pacijent pacijent;

    public Prijava(Long id, LocalDateTime datumIVreme, Vakcina vakcina, Pacijent pacijent) {
        this.id = id;
        this.datumIVreme = datumIVreme;
        this.vakcina = vakcina;
        this.pacijent = pacijent;
    }

    public Prijava() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDateTime getDatumIVreme() {
        return datumIVreme;
    }

    public void setDatumIVreme(LocalDateTime datumIVreme) {
        this.datumIVreme = datumIVreme;
    }

    public Vakcina getVakcina() {
        return vakcina;
    }

    public void setVakcina(Vakcina vakcina) {
        this.vakcina = vakcina;
    }

    public Pacijent getPacijent() {
        return pacijent;
    }

    public void setPacijent(Pacijent pacijent) {
        this.pacijent = pacijent;
    }

    @Override
    public String toString() {
        return "Prijava{" +
                "id=" + id +
                ", datumIVreme=" + datumIVreme +
                ", vakcina=" + vakcina +
                ", pacijent=" + pacijent +
                '}';
    }
    public Long getVakcinaId() {
        return vakcinaId;
    }

    public void setVakcinaId(Long vakcinaId) {
        this.vakcinaId = vakcinaId;
    }

    private Long vakcinaId;

    public Long getPatientId() {
        return pacijentId;
    }

    public void setPatientId(Long pacijentId) {
        this.pacijentId = pacijentId;
    }

    private Long pacijentId;
}
