package com.models;

public enum Uloga {
    MEDICINSKO_OSOBLJE,
    PACIJENT,
    ADMINISTRATOR
}
