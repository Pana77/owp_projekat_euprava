package com.models;


import org.springframework.scheduling.support.SimpleTriggerContext;

import java.time.LocalDate;
import java.time.LocalDateTime;

public class Korisnik {
    private Long id;
    private String ime;
    private String prezime;
    private String email;
    private String lozinka;
    private String adresa;
    private String JMBG;
    private LocalDate datumRodjenja;
    private String brojTelefona;
    private LocalDateTime datumIVremeRegistracije;
    private Uloga uloga;

    public Korisnik() {
    }

    public Korisnik(Long id, String ime, String prezime, String email, String lozinka, String adresa,
                    String JMBG, LocalDate datumRodjenja, String brojTelefona, LocalDateTime datumIVremeRegistracije, Uloga uloga) {
        this.id = id;
        this.ime = ime;
        this.prezime = prezime;
        this.email = email;
        this.lozinka = lozinka;
        this.adresa = adresa;
        this.JMBG = JMBG;
        this.datumRodjenja = datumRodjenja;
        this.brojTelefona = brojTelefona;
        this.datumIVremeRegistracije = datumIVremeRegistracije;
        this.uloga = uloga;
    }

    public Long getId() {
        return id;
    }

    public String getJMBG() {
        return JMBG;
    }

    public void setJMBG(String JMBG) {
        this.JMBG = JMBG;
    }

    public String getEmail(){
        return email;
    }

    public String getLozinka() {
        return lozinka;
    }

    public String getIme() {
        return ime;
    }

    public String getPrezime() {
        return prezime;
    }

    public LocalDate getDatumRodjenja() {
        return datumRodjenja;
    }

    public String getAdresa() {
        return adresa;
    }

    public String getBrojTelefona() {
        return brojTelefona;
    }

    public LocalDateTime getDatumIVremeRegistracije() {
        return datumIVremeRegistracije;
    }

    public Uloga getUloga() {
        return uloga;
    }

    public void setId(Long  id) {
        this.id = id;
    }

    public void setEmail(String email){
        this.email = email;
    }

    public void setLozinka(String lozinka) {
        this.lozinka = lozinka;
    }

    public void setIme(String ime) {
        this.ime = ime;
    }

    public void setPrezime(String prezime) {
        this.prezime = prezime;
    }

    public void setDatumRodjenja(LocalDate datumRodjenja) {
        this.datumRodjenja = datumRodjenja;
    }

    public void setAdresa(String adresa) {
        this.adresa = adresa;
    }

    public void setBrojTelefona(String brojTelefona) {
        this.brojTelefona = brojTelefona;
    }

    public void setDatumIVremeRegistracije(LocalDateTime datumIVremeRegistracije) {
        this.datumIVremeRegistracije = datumIVremeRegistracije;
    }

    public void setUloga(Uloga uloga) {
        this.uloga = uloga;
    }

    @Override
    public String toString() {
        return "Korisnik{" +
                "id=" + id +
                ", ime='" + ime + '\'' +
                ", prezime='" + prezime + '\'' +
                ", email='" + email + '\'' +
                ", lozinka=" + lozinka +
                ", adresa='" + adresa + '\'' +
                ", JMBG='" + JMBG + '\'' +
                ", datumRodjenja=" + datumRodjenja +
                ", brojTelefona=" + brojTelefona +
                ", datumIVremeRegistracije=" + datumIVremeRegistracije +
                ", uloga=" + uloga +
                '}';
    }
}
