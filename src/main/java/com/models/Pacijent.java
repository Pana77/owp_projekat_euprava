package com.models;

import java.time.LocalDateTime;

public class Pacijent {

    private Long pacijentId;
    private Boolean vakcinisan;
    private int primio;
    private LocalDateTime poslednjaDoza;
    public Korisnik korisnik;

    public Pacijent(Long pacijentId, Boolean vakcinisan, int primio, LocalDateTime poslednjaDoza, Korisnik korisnik) {
        this.pacijentId = pacijentId;
        this.vakcinisan = vakcinisan;
        this.primio = primio;
        this.poslednjaDoza = poslednjaDoza;
        this.korisnik = korisnik;
    }

    public Pacijent() {
    }

    public Long getPacijentId() {
        return pacijentId;
    }

    public void setPacijentId(Long pacijentId) {
        this.pacijentId = pacijentId;
    }

    public Boolean getVakcinisan() {
        return vakcinisan;
    }

    public void setVakcinisan(Boolean vakcinisan) {
        this.vakcinisan = vakcinisan;
    }

    public int getPrimio() {
        return primio;
    }

    public void setPrimio(int primio) {
        this.primio = primio;
    }

    public LocalDateTime getPoslednjaDoza() {
        return poslednjaDoza;
    }

    public void setPoslednjaDoza(LocalDateTime poslednjaDoza) {
        this.poslednjaDoza = poslednjaDoza;
    }

    public Korisnik getKorisnik() {
        return korisnik;
    }

    public void setKorisnik(Korisnik korisnik) {
        this.korisnik = korisnik;
    }

    @Override
    public String toString() {
        return "Pacijent{" +
                "pacijentId=" + pacijentId +
                ", vakcinisan=" + vakcinisan +
                ", primio=" + primio +
                ", poslednjaDoza=" + poslednjaDoza +
                ", korisnik=" + korisnik +
                '}';
    }
}
