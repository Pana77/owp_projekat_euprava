package com.repositories;

import com.dao.VestOObolelimaInterface;
import com.models.VestOObolelima;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.sql.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Repository
public class VestOObolelimaRepository implements VestOObolelimaInterface{
    @Autowired
    private JdbcTemplate jdbcTemplate;

    private class VestOObolelimaRowCallBackHandler implements RowCallbackHandler {

        private Map<Long, VestOObolelima> VestOObolelimaMap = new LinkedHashMap<>();

        @Override
        public void processRow(ResultSet resultSet) throws SQLException {
            int index = 1;
            Long id = resultSet.getLong(index++);
            Long brojObolelih = resultSet.getLong(index++);
            Long brojTestiranih = resultSet.getLong(index++);
            Long brojObolelihOdPocetka = resultSet.getLong(index++);
            Long brojHospitalizovanih = resultSet.getLong(index++);
            Long brojPacijenataNaRespiratorima = resultSet.getLong(index++);
            LocalDate datumObjavljivanja = resultSet.getDate(index++).toLocalDate();

            VestOObolelima vestiObolelih = VestOObolelimaMap.get(id);
            if (vestiObolelih == null) {
                vestiObolelih = new VestOObolelima(id, brojObolelih, brojTestiranih, brojObolelihOdPocetka, brojHospitalizovanih, brojPacijenataNaRespiratorima, datumObjavljivanja);
                VestOObolelimaMap.put(vestiObolelih.getId(), vestiObolelih);
            }
        }

        public List<VestOObolelima> getVestOObolelima() {
            return new ArrayList<>(VestOObolelimaMap.values());
        }

    }

    @Override
    public VestOObolelima findVestOObolelimaById(Long id) {
        String sql =
                "SELECT temp.id, temp.brojobolelih, temp.brojtestiranih, (SELECT brojobolelihodpocetka(temp.id)) AS total_infected, temp.datumobjavljivanja, temp.brojHospitalizovanih, temp.brojPacijenataNaRespiratorima" +
                        "FROM vestoobolelima temp " +
                        "WHERE temp.id = ? " +
                        "ORDER BY temp.id";

        VestOObolelimaRepository.VestOObolelimaRowCallBackHandler rowCallbackHandler = new VestOObolelimaRepository.VestOObolelimaRowCallBackHandler();
        jdbcTemplate.query(sql, rowCallbackHandler, id);

        if (rowCallbackHandler.getVestOObolelima().size() == 0) {
            return null;
        }
        return rowCallbackHandler.getVestOObolelima().get(0);
    }

    @Override
    public List<VestOObolelima> findAllVestiOObolelima() {
        String sql = "SELECT * FROM vestoobolelima";
        VestOObolelimaRepository.VestOObolelimaRowCallBackHandler rowCallbackHandler = new VestOObolelimaRepository.VestOObolelimaRowCallBackHandler();
        jdbcTemplate.query(sql, rowCallbackHandler);

        if (rowCallbackHandler.getVestOObolelima().size() == 0) {
            return null;
        }
        return rowCallbackHandler.getVestOObolelima();
    }

    @Transactional
    @Override

    public int save(VestOObolelima vestOObolelima) {
        PreparedStatementCreator preparedStatementCreator = new PreparedStatementCreator() {

            @Override
            public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
                String sql = "INSERT INTO vestoobolelima  (brojobolelih, brojtestiranih, brojhospitalizovanih, brojpacijenatanarespiratorima, datumobjavljivanja) VALUES (?, ?, ?, ?, ?)";

                PreparedStatement preparedStatement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
                int index = 1;
                preparedStatement.setLong(index++, vestOObolelima.getBrojObolelih());
                preparedStatement.setLong(index++, vestOObolelima.getBrojTestiranih());
                preparedStatement.setLong(index++, vestOObolelima.getBrojHospitalizovanih());
                preparedStatement.setLong(index++, vestOObolelima.getBrojPacijenataNaRespiratorima());
                preparedStatement.setDate(index++, Date.valueOf(vestOObolelima.getDatumObjavljivanja()));
                return preparedStatement;
            }

        };
        GeneratedKeyHolder keyHolder = new GeneratedKeyHolder();
        boolean success = jdbcTemplate.update(preparedStatementCreator, keyHolder) == 1;
        return success ? 1 : 0;
    }

    @Transactional
    @Override
    public int update(VestOObolelima vestOObolelima) {
        String sql = "UPDATE vestoobolelima SET vestoobolelima = ?, brojtestiranih = ?, brojhospitalizovanih = ?, brojPacijenataNaRespiratorima = ?, datumobjavljivanja = ? WHERE id = ?";
        boolean success = jdbcTemplate.update(sql,
                vestOObolelima.getBrojObolelih(),
                vestOObolelima.getBrojTestiranih(),
                vestOObolelima.getBrojHospitalizovanih(),
                vestOObolelima.getBrojPacijenataNaRespiratorima(),
                vestOObolelima.getDatumObjavljivanja(),
                vestOObolelima.getId()) == 1;

        return success ? 1 : 0;
    }

    @Transactional
    @Override
    public int delete(Long id) {
        String sql = "DELETE FROM vestoobolelima WHERE id = ?";
        return jdbcTemplate.update(sql, id);
    }
}