package com.repositories;

import com.dao.ProizvodjacVakcineInterface;
import com.models.ProizvodjacVakcine;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.sql.*;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Repository
public class ProizvodjacVakcineRepository implements ProizvodjacVakcineInterface {
    @Autowired
    private JdbcTemplate jdbcTemplate;

    private class ProizvodjacVakcineRowCallBackHandler implements RowCallbackHandler {
        private Map<Long, ProizvodjacVakcine> SviProizvodjaciVakcine = new LinkedHashMap<>();
        @Override
        public void processRow(ResultSet data) throws SQLException {
            int index = 1;
            Long id = data.getLong(index++);
            String ime = data.getString(index++);
            String drzavaProizvodnje = data.getString(index++);
            ProizvodjacVakcine temp = SviProizvodjaciVakcine.get(id);
            if (temp == null) {
                temp = new ProizvodjacVakcine(id, ime, drzavaProizvodnje);
                SviProizvodjaciVakcine.put(temp.getId(), temp);
            }
        }
        public List<ProizvodjacVakcine> getProizvodjaceVakcina() {
            return new ArrayList<>(SviProizvodjaciVakcine.values());
        }
    }

    @Override
    public List<ProizvodjacVakcine> findAllProizvodjaceVakcina() {
        String command =
                "SELECT proizvodjacvakcina.id, proizvodjacvakcina.ime, proizvodjacvakcina.drzavaproizvodnje " +
                        "FROM proizvodjacvakcina proizvodjacvakcina " +
                        "ORDER BY proizvodjacvakcina.id";
        ProizvodjacVakcineRowCallBackHandler rowCallbackHandler = new ProizvodjacVakcineRowCallBackHandler();
        jdbcTemplate.query(command, rowCallbackHandler);
        if (rowCallbackHandler.getProizvodjaceVakcina().size() == 0) {
            return null;
        }
        return rowCallbackHandler.getProizvodjaceVakcina();
    }
    @Override
    public ProizvodjacVakcine findProizvodjacaVakcinaById(Long id) {
        String command =
                "SELECT proizvodjacvakcina.id, proizvodjacvakcina.name, proizvodjacvakcina.country " +
                        "FROM proizvodjacvakcina proizvodjacvakcina " +
                        "WHERE proizvodjacvakcina.id = ? " +
                        "ORDER BY proizvodjacvakcina.id";

        ProizvodjacVakcineRowCallBackHandler rowCallbackHandler = new ProizvodjacVakcineRowCallBackHandler();
        jdbcTemplate.query(command, rowCallbackHandler, id);
        if (rowCallbackHandler.getProizvodjaceVakcina().size() == 0) {
            return null;
        }
        return rowCallbackHandler.getProizvodjaceVakcina().get(0);
    }
    @Transactional
    @Override
    public int save(ProizvodjacVakcine proizvodjacVakcine) {
        PreparedStatementCreator preparedStatementCreator = new PreparedStatementCreator() {

            @Override
            public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
                String command = "INSERT INTO proizvodjacVakcine (ime, drzavaproizvodnje) VALUES (?, ?)";

                PreparedStatement statement = connection.prepareStatement(command, Statement.RETURN_GENERATED_KEYS);
                int index = 1;
                statement.setString(index++, proizvodjacVakcine.getIme());
                statement.setString(index++, proizvodjacVakcine.getDrzavaProizvodnje());
                return statement;
            }

        };
        GeneratedKeyHolder keyHolder = new GeneratedKeyHolder();
        boolean success = jdbcTemplate.update(preparedStatementCreator, keyHolder) == 1;
        return success ? 1 : 0;
    }
    @Transactional
    @Override
    public int update(ProizvodjacVakcine proizvodjacVakcine) {
        String command = "UPDATE proizvodjacVakcine SET ime = ?, drzavaproizvodnje = ? WHERE id = ?";
        boolean success = jdbcTemplate.update(command,
                proizvodjacVakcine.getIme(),
                proizvodjacVakcine.getDrzavaProizvodnje(),
                proizvodjacVakcine.getId()) == 1;
        return success ? 1 : 0;
    }
    @Transactional
    @Override
    public int delete(Long id) {
        String command = "DELETE FROM proizvodjacVakcine WHERE id = ?";
        return jdbcTemplate.update(command, id);
    }

}