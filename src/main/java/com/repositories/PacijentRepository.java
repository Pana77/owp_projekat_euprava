package com.repositories;

import com.dao.KorisnikInterface;
import com.dao.PacijentInterface;
import com.models.Korisnik;
import com.models.Pacijent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.sql.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
@Repository
public class PacijentRepository implements PacijentInterface {
    @Autowired
    private JdbcTemplate jdbcTemplate;
    @Autowired
    private KorisnikInterface repo;

    private class PacijentRowCallBackHandler implements RowCallbackHandler {
        private Map<Long, Pacijent> sviPacijenti = new LinkedHashMap<>();

        @Override
        public void processRow(ResultSet resultSet) throws SQLException {
            int index = 1;
            Long id = resultSet.getLong(index++);
            Boolean vakcinisan = resultSet.getBoolean(index++);
            int primio = resultSet.getInt(index++);
            Timestamp poslednjaDoza = resultSet.getTimestamp(index++);
            LocalDateTime poslednjaDozaVreme = null;

            if (poslednjaDoza != null) {
                poslednjaDozaVreme = poslednjaDoza.toLocalDateTime();
            }
            Korisnik korisnik = repo.findKorisnikById(id);

            if (sviPacijenti.get(id) == null) {
                Pacijent pacijent = new Pacijent(id, vakcinisan, primio, poslednjaDozaVreme, korisnik);
                sviPacijenti.put(pacijent.getPacijentId(), pacijent);
            } else {
                Pacijent pacijent = sviPacijenti.get(id);
            }



        }
        public List<Pacijent> getPacijente() {
            return new ArrayList<>(sviPacijenti.values());
        }

    }
    @Override
    public Pacijent findPacijentById(Long id) {
        String sql =
                "SELECT temp.pacijentid, temp.vakcinisan, temp.primio, temp.poslednjadoza " +
                        "FROM pacijentinfo temp " +
                        "WHERE temp.pacijentid = ? " +
                        "ORDER BY temp.pacijentid";

        PacijentRepository.PacijentRowCallBackHandler rowCallBackHandler = new PacijentRepository.PacijentRowCallBackHandler();
        jdbcTemplate.query(sql, rowCallBackHandler, id);
        if(rowCallBackHandler.getPacijente().size() == 0) {
            return null;
        }
        return rowCallBackHandler.getPacijente().get(0);
    }

    @Override
    public List<Pacijent> findAllPacijenti(){
        String sql =
                "SELECT temp.pacijentid, temp.vakcinisan, temp.primio, temp.poslednjadoza" +
                        "FROM pacijentinfo temp" +
                        "ORDER BY temp.pacijentid";

        PacijentRepository.PacijentRowCallBackHandler rowCallBackHandler = new PacijentRepository.PacijentRowCallBackHandler();
        jdbcTemplate.query(sql, rowCallBackHandler);

        if(rowCallBackHandler.getPacijente().size() == 0){
            return null;
        }
        return rowCallBackHandler.getPacijente();
    }

    @Transactional
    @Override
    public int save(Pacijent pacijent){
        PreparedStatementCreator preparedStatementCreator = new PreparedStatementCreator() {
            @Override
            public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
                String sql = "INSERT INTO pacijentinfo(pacijentid, vakcinisan, primio, poslednjadoza) values (?, ?, ?, ?)";
                PreparedStatement preparedStatement = con.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
                int index = 1;
                if(pacijent.getVakcinisan() != null){
                    preparedStatement.setBoolean(index++, pacijent.getVakcinisan());
                }else{
                    preparedStatement.setBoolean(index++, false);
                }
                if(pacijent.getPrimio() != 0){
                    preparedStatement.setInt(index++, pacijent.getPrimio());
                }else{
                    preparedStatement.setInt(index++, 0);
                }
                if(pacijent.getPoslednjaDoza() != null) {
                    Timestamp timestamp = Timestamp.valueOf(pacijent.getPoslednjaDoza());
                    preparedStatement.setString(index++, timestamp.toString());
                }else{
                    preparedStatement.setString(index++, "1000-1-1 1:1:1");
                }
                preparedStatement.setLong(index++, pacijent.getPacijentId());

                return preparedStatement;
            }
        };
        GeneratedKeyHolder keyHolder = new GeneratedKeyHolder();
        boolean success = jdbcTemplate.update(preparedStatementCreator, keyHolder) == 1;
        return success ? 1:0;
    }

    @Transactional
    @Override
    public int update(Pacijent pacijent) {
        String sql = "UPDATE pacijentinfo SET poslednjadoza = ?, vakcinisan = ?, primio = ? WHERE pacijentid = ?";
        Pacijent temp = pacijent;
        if(temp.getVakcinisan() == false){
            temp.setPrimio(1);
            temp.setVakcinisan(true);
            temp.setPoslednjaDoza(LocalDateTime.now());

        }else if(temp.getPrimio() == 1){
            if(temp.getPoslednjaDoza().plusMonths(3).isBefore(LocalDateTime.now())){
                temp.setPrimio(2);
                temp.setPoslednjaDoza(LocalDateTime.now());
            }
        }else if(temp.getPrimio() == 2){
            if(temp.getPoslednjaDoza().plusMonths(6).isBefore(LocalDateTime.now())){
                temp.setPrimio(3);
                temp.setPoslednjaDoza(LocalDateTime.now());
            }
        }else if(temp.getPrimio() == 3){
            if(temp.getPoslednjaDoza().plusMonths(3).isBefore(LocalDateTime.now())){
                temp.setPrimio(4);
                temp.setPoslednjaDoza(LocalDateTime.now());
            }

        }
        boolean success = jdbcTemplate.update(sql,
                temp.getVakcinisan(),
                temp.getPrimio(),
                temp.getPoslednjaDoza(),
                temp.getPacijentId()) == 1;
        return success ? 1 : 0;
    }

    @Transactional
    @Override
    public int delete(Long id) {
        String sql = "DELETE FROM pacijentinfo WHERE pacijentid = ?";
        return jdbcTemplate.update(sql, id);
    }

}

