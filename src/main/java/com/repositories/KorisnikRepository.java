package com.repositories;

import com.dao.KorisnikInterface;
import com.models.Korisnik;
import com.models.Uloga;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.sql.*;
import java.time.LocalDateTime;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Repository
public class KorisnikRepository implements KorisnikInterface {
    @Autowired
    private JdbcTemplate jdbcTemplate;

    private class UserRowCallBackHandler implements RowCallbackHandler {
        private Map<Long, Korisnik> sviKorisnici = new LinkedHashMap<>();

        @Override
        public void processRow(ResultSet resultSet) throws SQLException {
            int index = 1;
            Long id = resultSet.getLong(index++);
            String ime = resultSet.getString(index++);
            String prezime = resultSet.getString(index++);
            String email = resultSet.getString(index++);
            String lozinka = resultSet.getString(index++);
            String adresa = resultSet.getString(index++);
            String JMBG = resultSet.getString(index++);
            LocalDate datumRodjenja = resultSet.getDate(index++).toLocalDate();
            String brojTelefona = resultSet.getString(index++);
            LocalDateTime datumIVremeRegistracije = resultSet.getTimestamp(index++).toLocalDateTime();
            Uloga uloga = Uloga.valueOf(resultSet.getString(index++));

            Korisnik korisnik = sviKorisnici.get(id);
            if (korisnik == null) {
                korisnik = new Korisnik(id, ime, prezime, email, lozinka, adresa, JMBG,
                        datumRodjenja, brojTelefona, datumIVremeRegistracije, uloga);
                sviKorisnici.put(korisnik.getId(), korisnik);
            }
        }
        public List<Korisnik> getKorisnici(){
            return new ArrayList<>(sviKorisnici.values());
        }
    }
    @Override
    public List<Korisnik> findAllKorisnici() {
        String sql = "SELECT temp.id, temp.ime, temp.prezime, temp.email, temp.lozinka, temp.adresa, temp.JMBG, temp.datumRodjenja, temp.brojTelefona, temp.datumIVremeRegistracije, temp.uloga " +
                "FROM korisnici temp " +
                "ORDER BY temp.id";

        UserRowCallBackHandler rowCallBackHandler = new UserRowCallBackHandler();
        jdbcTemplate.query(sql, rowCallBackHandler);

        if (rowCallBackHandler.getKorisnici().size() == 0) {
            return null;
        }

        return rowCallBackHandler.getKorisnici();
    }
    @Override
    public Korisnik findKorisnikById(Long id) {
        String sql = "SELECT temp.id, temp.ime, temp.prezime, temp.email, temp.lozinka, temp.adresa, temp.JMBG, temp.datumRodjenja, temp.brojTelefona, temp.datumIVremeRegistracije, temp.uloga " +
                "FROM korisnici temp " +
                "WHERE temp.id=?" +
                "ORDER BY temp.id";
        UserRowCallBackHandler rowCallBackHandler = new UserRowCallBackHandler();
        jdbcTemplate.query(sql, rowCallBackHandler, id);

        if (rowCallBackHandler.getKorisnici().size() == 0) {
            return null;
        }

        return rowCallBackHandler.getKorisnici().get(0);
    }
    @Override
    public Korisnik findKorisnikByEmail(String email) {
        String sql = "SELECT temp.id, temp.ime, temp.prezime, temp.email, temp.lozinka, temp.adresa, temp.JMBG, temp.datumRodjenja, temp.brojTelefona, temp.datumIVremeRegistracije, temp.uloga " +
                "FROM korisnici temp " +
                "WHERE temp.email = ? " +
                "ORDER BY temp.id";

        UserRowCallBackHandler rowCallBackHandler = new UserRowCallBackHandler();
        jdbcTemplate.query(sql, rowCallBackHandler, email);

        if (rowCallBackHandler.getKorisnici().isEmpty()) {
            return null;
        }

        return rowCallBackHandler.getKorisnici().get(0);
    }

    @Override
    public Korisnik findKorisnikWithCredentials(String email, String lozinka){
        String sql = "SELECT temp.id, temp.ime, temp.prezime, temp.email, temp.lozinka, temp.adresa, temp.JMBG, temp.datumRodjenja, temp.brojTelefona, temp.datumIVremeRegistracije, temp.uloga " +
                "FROM korisnici temp " +
                "WHERE temp.email = ? AND" +
                "temp.lozinka = ?" +
                "ORDER BY temp.id";
        UserRowCallBackHandler rowCallBackHandler = new UserRowCallBackHandler();
        jdbcTemplate.query(sql, rowCallBackHandler, email, lozinka);

        if (rowCallBackHandler.getKorisnici().size() == 0) {
            return null;
        }

        return rowCallBackHandler.getKorisnici().get(0);
    }
    @Transactional
    @Override
    public int save(Korisnik korisnik) {
        PreparedStatementCreator preparedStatementCreator = new PreparedStatementCreator() {
            @Override
            public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
                String sql = "INSERT INTO korisnici (ime, prezime, email, lozinka, adresa, jmbg, datumRodjenja, brojTelefona, datumIVremeRegistracije, uloga) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?)";
                PreparedStatement preparedStatement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
                int index = 1;
                preparedStatement.setString(index++, korisnik.getIme());
                preparedStatement.setString(index++, korisnik.getPrezime());
                preparedStatement.setString(index++, korisnik.getEmail());
                preparedStatement.setString(index++, korisnik.getLozinka());
                preparedStatement.setString(index++, korisnik.getAdresa());
                preparedStatement.setString(index++, korisnik.getJMBG());
                java.sql.Date datum = Date.valueOf(korisnik.getDatumRodjenja());
                preparedStatement.setString(index++, datum.toString());
                preparedStatement.setString(index++, korisnik.getBrojTelefona());
                Timestamp stampRegistracije = Timestamp.valueOf(korisnik.getDatumIVremeRegistracije());
                preparedStatement.setString(index++, stampRegistracije.toString());
                preparedStatement.setString(index++, korisnik.getUloga().toString());

                return preparedStatement;

            }

        };
        GeneratedKeyHolder keyHolder = new GeneratedKeyHolder();
        boolean uspesno = jdbcTemplate.update(preparedStatementCreator, keyHolder) == 1;
        return uspesno ? 1 : 0;
    }
    @Transactional
    @Override
    public int update(Korisnik korisnik) {
        String sql = "UPDATE korisnici SET ime = ?, prezime = ?, email = ?, lozinka = ?, adresa = ?, jmbg = ?, datumRodjenja = ?, brojTelefona = ?, datumIVremeRegistracije = ?, uloga = ? WHERE id = ?";
        boolean uspesno = jdbcTemplate.update(sql,
                korisnik.getIme(), korisnik.getPrezime(), korisnik.getEmail(), korisnik.getLozinka(), korisnik.getAdresa(), korisnik.getJMBG(), korisnik.getDatumRodjenja(), korisnik.getBrojTelefona(), korisnik.getDatumIVremeRegistracije(), korisnik.getUloga().toString(), korisnik.getId()) == 1;
        return uspesno ? 1 : 0;

    }
    @Transactional
    @Override
    public int delete(Long id){
        String sql = "DELETE FROM korisnici WHERE id = ?";
        return jdbcTemplate.update(sql, id);
    }
}

