package com.repositories;

import com.dao.VestInterface;
import com.models.Vest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.sql.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Repository
public class VestRepository implements VestInterface {
    @Autowired
    private JdbcTemplate jdbcTemplate;

    private class VestRowCallBackHandler implements RowCallbackHandler {
        private Map<Long, Vest> sveVesti = new LinkedHashMap<>();

        @Override
        public void processRow(ResultSet resultSet) throws SQLException {
            int index = 1;
            Long id = resultSet.getLong(index++);
            String naziv = resultSet.getString(index++);
            String sadrzaj = resultSet.getString(index++);
            LocalDateTime datumIVremeObjavljivanja = resultSet.getTimestamp(index++).toLocalDateTime();
            Vest vest = sveVesti.get(id);
            if (vest == null) {
                vest = new Vest(id, naziv, sadrzaj, datumIVremeObjavljivanja);
                sveVesti.put(vest.getId(), vest);
            }
        }

        public List<Vest> getVest() {
            return new ArrayList<>(sveVesti.values());
        }

    }

    @Override
    public Vest findVestById(Long id) {
        String sql =
                "SELECT temp.id, temp.naziv, temp.sadrzaj, temp.datumivremeobjavljivanja " +
                        "FROM vesti temp " +
                        "WHERE temp.id = ? ";

        VestRepository.VestRowCallBackHandler rowCallbackHandler = new VestRowCallBackHandler();
        jdbcTemplate.query(sql, rowCallbackHandler, id);

        if (rowCallbackHandler.getVest().size() == 0) {
            return null;
        }
        return rowCallbackHandler.getVest().get(0);
    }


    @Override
    public List<Vest> findAllVesti() {
        String sql =
                "SELECT temp.id, temp.naziv, temp.sadrzaj, temp.datumivremeobjavljivanja " +
                        "FROM vesti temp ";

        VestRepository.VestRowCallBackHandler rowCallbackHandler = new VestRowCallBackHandler();
        jdbcTemplate.query(sql, rowCallbackHandler);

        if (rowCallbackHandler.getVest().size() == 0) {
            return null;
        }
        return rowCallbackHandler.getVest();
    }

    @Transactional
    @Override
    public int save(Vest vest) {
        PreparedStatementCreator preparedStatementCreator = new PreparedStatementCreator() {

            @Override
            public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
                String sql = "INSERT INTO vesti (naziv, sadrzaj, datumivremeobjavljivanja) VALUES (?, ?, ?)";

                PreparedStatement preparedStatement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
                int index = 1;
                preparedStatement.setString(index++, vest.getNaziv());
                preparedStatement.setString(index++, vest.getSadrzaj());
                Timestamp timestamp = Timestamp.valueOf(vest.getDatumIVremeObjavljivanja());
                preparedStatement.setString(index++, timestamp.toString());
                return preparedStatement;
            }

        };
        GeneratedKeyHolder keyHolder = new GeneratedKeyHolder();
        boolean success = jdbcTemplate.update(preparedStatementCreator, keyHolder) == 1;
        return success ? 1 : 0;
    }

    @Transactional
    @Override
    public int update(Vest vest) {
        String sql = "UPDATE vesti SET naziv = ?, sadrzaj = ?, datumivremeobjavljivanja = ? WHERE id = ?";
        boolean success = jdbcTemplate.update(sql,
                vest.getNaziv(),
                vest.getSadrzaj(),
                Timestamp.valueOf(vest.getDatumIVremeObjavljivanja()).toString(),
                vest.getId()) == 1;

        return success ? 1 : 0;
    }

    @Transactional
    @Override
    public int delete(Long id) {
        String sql = "DELETE FROM vesti WHERE id = ?";
        return jdbcTemplate.update(sql, id);
    }

}
