package com.repositories;

import com.dao.PrijavaInterface;
import com.models.Pacijent;
import com.models.Prijava;
import com.models.Vakcina;
import com.services.PacijentService;
import com.services.VakcinaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.sql.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
@Repository
public class PrijavaRepository implements PrijavaInterface {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private PacijentService pacijentService;

    @Autowired
    private VakcinaService vakcinaService;

    private class PrijavaRowCallBackHandler implements RowCallbackHandler {
        private Map<Long, Prijava> svePrijave = new LinkedHashMap<>();
        @Override
        public void processRow(ResultSet data) throws SQLException {
            int index = 1;
            Long id = data.getLong(index++);
            LocalDateTime datumIVreme = data.getTimestamp(index++).toLocalDateTime();
            Long pacijentId = data.getLong(index++);
            Long vakcinaId = data.getLong(index++);
            Pacijent pacijent = pacijentService.getPacijentById(pacijentId);
            Vakcina vakcina = vakcinaService.get(vakcinaId);
            Prijava temp = svePrijave.get(id);
            if (temp == null) {
                temp = new Prijava(id, datumIVreme, vakcina, pacijent);
                svePrijave.put(temp.getId(), temp);
            }
        }
        public List<Prijava> getPrijave() {
            return new ArrayList<>(svePrijave.values());
        }
    }
    @Override
    public List<Prijava> findAllPrijave() {
        String command =
                "SELECT prijava.id, prijava.datumivreme, prijava.pacijent, prijava.vakcina " +
                        "FROM prijave prijava " +
                        "ORDER BY prijava.id";
        PrijavaRowCallBackHandler rowCallbackHandler = new PrijavaRowCallBackHandler();
        jdbcTemplate.query(command, rowCallbackHandler);
        if (rowCallbackHandler.getPrijave().size() == 0) {
            return null;
        }
        return rowCallbackHandler.getPrijave();
    }

    @Override
    public Prijava findPrijavaById(Long id) {
        String command =
                "SELECT prijava.id, prijava.datumivreme, prijava.pacijent, prijava.vakcina " +
                        "FROM prijave prijava " +
                        "WHERE prijava.id = ? " +
                        "ORDER BY prijava.id";

        PrijavaRowCallBackHandler rowCallbackHandler = new PrijavaRowCallBackHandler();
        jdbcTemplate.query(command, rowCallbackHandler, id);
        if (rowCallbackHandler.getPrijave().size() == 0) {
            return null;
        }
        return rowCallbackHandler.getPrijave().get(0);
    }
    @Override
    public List<Prijava> findPrijavaByPacijentId(Long id) {
        String command =
                "SELECT prijava.id, prijava.datumivreme, prijava.pacijent, prijava.vakcina " +
                        "FROM prijave prijava " +
                        "WHERE prijava.pacijent = ? " +
                        "ORDER BY prijava.id";

        PrijavaRowCallBackHandler rowCallbackHandler = new PrijavaRowCallBackHandler();
        jdbcTemplate.query(command, rowCallbackHandler, id);
        if (rowCallbackHandler.getPrijave().size() == 0) {
            return null;
        }
        return rowCallbackHandler.getPrijave();
    }

    @Override
    public List<Prijava> searchPrijave(String query) {
        String sql =
                "SELECT prijava.id, prijava.datumivreme, prijava.pacijent, v.vakcina " +
                        "FROM prijave prijava" +
                        "JOIN korisnici k ON prijava.pacijent = k.id " +
                        "WHERE k.ime LIKE ? OR k.prezime LIKE ? OR k.jmbg LIKE ?";

        PrijavaRepository.PrijavaRowCallBackHandler rowCallbackHandler = new PrijavaRepository.PrijavaRowCallBackHandler();
        String likeQuery = "%" + query + "%";
        jdbcTemplate.query(sql, rowCallbackHandler, query, query, query);

        return rowCallbackHandler.getPrijave();
    }

    @Transactional
    @Override
    public List<Prijava> findAllPrijaveByKorisnik(String searchTerm){
        String command = "SELECT prijava.id, prijava.datumivreme, prijava.pacijent, prijava.vakcina " +
                "FROM prijave prijava " +
                "JOIN pacijenti p ON prijava.pacijentid = p.id " +
                "WHERE p.korisnikime LIKE '%" + searchTerm + "%' " +
                "   OR p.prezime LIKE '%" + searchTerm + "%' " +
                "   OR p.jmbg LIKE '%" + searchTerm + "%' " +
                "ORDER BY prijava.id";
        PrijavaRowCallBackHandler rowCallbackHandler = new PrijavaRowCallBackHandler();
        jdbcTemplate.query(command, rowCallbackHandler, searchTerm);
        if (rowCallbackHandler.getPrijave().size() == 0) {
            return null;
        }
        return rowCallbackHandler.getPrijave();

    }
    @Transactional
    @Override
    public int save(Prijava prijava) {
        PreparedStatementCreator preparedStatementCreator = new PreparedStatementCreator() {

            @Override
            public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
                String command = "INSERT INTO prijave (datumivreme, pacijent, vakcina) VALUES (?, ?, ?)";

                PreparedStatement statement = connection.prepareStatement(command, Statement.RETURN_GENERATED_KEYS);
                int index = 1;
                Timestamp timestamp = Timestamp.valueOf(prijava.getDatumIVreme());
                statement.setString(index++, timestamp.toString());
                statement.setLong(index++, prijava.getPacijent().getPacijentId());
                statement.setLong(index++, prijava.getVakcina().getId());
                return statement;
            }
        };
        GeneratedKeyHolder keyHolder = new GeneratedKeyHolder();
        boolean success = jdbcTemplate.update(preparedStatementCreator, keyHolder) == 1;
        return success ? 1 : 0;
    }
    @Transactional
    @Override
    public int update(Prijava prijava) {
        String command = "UPDATE proizvodjacvakcine SET datumivreme = ?, pacijent = ?, vakcina = ? WHERE id = ?";
        boolean success = jdbcTemplate.update(command,
                Timestamp.valueOf(prijava.getDatumIVreme()).toString(),
                prijava.getPacijent().getPacijentId(),
                prijava.getVakcina().getId(),
                prijava.getId()) == 1;
        return success ? 1 : 0;
    }
    @Transactional

    @Override
    public int delete(Long id) {
        String command = "DELETE FROM prijave WHERE id = ?";
        return jdbcTemplate.update(command, id);
    }

}