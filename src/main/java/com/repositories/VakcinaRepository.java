package com.repositories;

import com.dao.VakcinaInterface;
import com.models.ProizvodjacVakcine;
import com.models.Vakcina;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.sql.*;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Repository
public class VakcinaRepository implements VakcinaInterface {

    @Autowired
    private JdbcTemplate jdbcTemplate;
    @Autowired
    private ProizvodjacVakcineRepository proizvodjacVakcineRepo;

    private class VakcinaRowCallbackHandler implements RowCallbackHandler {

        private Map<Long, Vakcina> vakcine = new LinkedHashMap<>();

        @Override
        public void processRow(ResultSet resultSet) throws SQLException {
            int index = 1;
            Long id = resultSet.getLong(index++);
            String ime = resultSet.getString(index++);
            Long dostupnaKolicina = resultSet.getLong(index++);
            Long proizvodjacId = resultSet.getLong(index++);

            ProizvodjacVakcine proizvodjacVakcine = proizvodjacVakcineRepo.findProizvodjacaVakcinaById(proizvodjacId);

            Vakcina vakcina = vakcine.get(id);
            if (vakcina == null) {
                vakcina = new Vakcina(id, ime, dostupnaKolicina, proizvodjacVakcine, proizvodjacId);
                vakcine.put(vakcina.getId(), vakcina);
            }
        }

        public List<Vakcina> getVakcine() { return new ArrayList<>(vakcine.values()); }
    }
    @Override
    public List<Vakcina> searchVakcine(String query) {
        String sql =
                "SELECT v.id, v.ime, v.dostupnakolicina, v.proizvodjacvakcine " +
                        "FROM vakcina v " +
                        "JOIN proizvodjacvakcine p ON v.proizvodjacvakcine = p.id " +
                        "WHERE v.ime LIKE ? OR p.ime LIKE ? OR p.drzava LIKE ?";

        VakcinaRowCallbackHandler rowCallbackHandler = new VakcinaRowCallbackHandler();
        String likeQuery = "%" + query + "%";
        jdbcTemplate.query(sql, rowCallbackHandler, query, query, query);

        return rowCallbackHandler.getVakcine();
    }
    @Override
    public List<Vakcina> findSortedVakcine(String sort, String direction) {
        String sql = "SELECT temp.id, temp.ime, temp.dostupnakolicina, temp.proizvodjacvakcine " +
                "FROM vakcina temp " +
                "ORDER BY " + sort + " " + direction;

        VakcinaRowCallbackHandler rowCallbackHandler = new VakcinaRowCallbackHandler();
        jdbcTemplate.query(sql, rowCallbackHandler);

        if (rowCallbackHandler.getVakcine().isEmpty()) {
            return null;
        }
        return rowCallbackHandler.getVakcine();
    }
    @Override
    public Vakcina findVakcinaById(Long id) {
        String sql =
                "SELECT temp.id, temp.ime, temp.dostupnakolicina, temp.proizvodjacvakcine " +
                        "FROM vakcina temp " +
                        "WHERE temp.id = ? " +
                        "ORDER BY temp.id";

        VakcinaRowCallbackHandler rowCallbackHandler = new VakcinaRowCallbackHandler();
        jdbcTemplate.query(sql, rowCallbackHandler, id);

        if(rowCallbackHandler.getVakcine().size() == 0) {
            return null;
        }
        return rowCallbackHandler.getVakcine().get(0);
    }

    @Override
    public List<Vakcina> findVakcinaByProizvodjacVakcine(ProizvodjacVakcine proizvodjacVakcine) {
        String sql =
                "SELECT temp.id, temp.ime, temp.dostupnakolicina, temp.proizvodjacvakcine" +
                        " FROM vakcina temp " +
                        "WHERE temp.proizvodjacvakcine = ? " +
                        "ORDER BY temp.id";

        VakcinaRowCallbackHandler rowCallbackHandler = new VakcinaRowCallbackHandler();
        jdbcTemplate.query(sql, rowCallbackHandler, proizvodjacVakcine.getId());

        if(rowCallbackHandler.getVakcine().size() == 0) {
            return null;
        }
        return rowCallbackHandler.getVakcine();
    }

    @Override
    public List<Vakcina> findAllVakcine() {
        String sql =
                "SELECT temp.id, temp.ime, temp.dostupnakolicina, temp.proizvodjacvakcine " +
                        "FROM vaccine temp " +
                        "ORDER BY temp.id";

        VakcinaRowCallbackHandler rowCallbackHandler = new VakcinaRowCallbackHandler();
        jdbcTemplate.query(sql, rowCallbackHandler);

        if(rowCallbackHandler.getVakcine().size() == 0) {
            return null;
        }
        return rowCallbackHandler.getVakcine();
    }

    @Override
    public List<Vakcina> findAllVakcineQuantity() {
        String sql = "SELECT temp.id, temp.ime, temp.dostupnakolicina, temp.proizvodjacvakcine " +
                "FROM vakcina temp " +
                "WHERE temp.dostupnakolicina > 0 " +
                "ORDER BY temp.id";

        VakcinaRowCallbackHandler rowCallbackHandler = new VakcinaRowCallbackHandler();
        jdbcTemplate.query(sql, rowCallbackHandler);

        if(rowCallbackHandler.getVakcine().size() == 0) {
            return null;
        }
        return rowCallbackHandler.getVakcine();
    }

    @Transactional
    @Override
    public int save(Vakcina vakcina) {
        PreparedStatementCreator preparedStatementCreator = new PreparedStatementCreator() {
            @Override
            public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
                String sql = "INSERT INTO vaccine (name, available, manufacturer) values (?, ?, ?)";
                PreparedStatement preparedStatement = con.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
                int index = 1;
                preparedStatement.setString(index++, vakcina.getIme());
                preparedStatement.setLong(index++, vakcina.getDostupnaKolicina());
                preparedStatement.setLong(index, vakcina.getProizvodjacVakcine().getId());

                return preparedStatement;
            }
        };

        GeneratedKeyHolder keyHolder = new GeneratedKeyHolder();
        boolean success = jdbcTemplate.update(preparedStatementCreator, keyHolder) == 1;
        return  success ? 1 : 0;
    }

    @Transactional
    @Override
    public int update(Vakcina vakcina) {
        String sql = "UPDATE vakcina SET ime = ?, dostupnakolicina = ?, proizvodjacvakcine = ? WHERE id = ?";
        boolean success = jdbcTemplate.update(sql,
                vakcina.getIme(),
                vakcina.getDostupnaKolicina(),
                vakcina.getProizvodjacVakcine().getId(),
                vakcina.getId()) == 1;
        return success ? 1 : 0;
    }

    @Transactional
    @Override
    public int delete(Long id) {
        String sql = "DELETE FROM vakcina WHERE id = ?";
        return jdbcTemplate.update(sql, id);
    }
}
