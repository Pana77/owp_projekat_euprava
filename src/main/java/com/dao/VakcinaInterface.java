package com.dao;

import com.models.ProizvodjacVakcine;
import com.models.Vakcina;

import java.util.List;

public interface VakcinaInterface {
    public List<Vakcina> findAllVakcine();
    public Vakcina findVakcinaById(Long id);
    public List<Vakcina> findVakcinaByProizvodjacVakcine(ProizvodjacVakcine proizvodjacVakcine);
    public List<Vakcina> searchVakcine(String query);
    public List<Vakcina> findSortedVakcine(String sort, String direction);
    public List<Vakcina> findAllVakcineQuantity();
    public int save(Vakcina vakcina);
    public int update(Vakcina vakcina);
    public int delete(Long id);
}
