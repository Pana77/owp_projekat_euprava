package com.dao;

import com.models.VestOObolelima;

import java.util.List;

public interface VestOObolelimaInterface {
    List<VestOObolelima> findAllVestiOObolelima();
    VestOObolelima findVestOObolelimaById(Long id);
    int save(VestOObolelima vestOObolelima);
    int update(VestOObolelima vestOObolelima);
    int delete(Long id);
}

