package com.dao;

import com.models.ProizvodjacVakcine;

import java.util.List;

public interface ProizvodjacVakcineInterface {
    public List<ProizvodjacVakcine> findAllProizvodjaceVakcina();
    public ProizvodjacVakcine findProizvodjacaVakcinaById(Long id);
    public int save(ProizvodjacVakcine proizvodjacVakcine);
    public int update(ProizvodjacVakcine proizvodjacVakcine);
    public int delete(Long id);
}
