package com.dao;

import com.models.Vest;

import java.util.List;

public interface VestInterface {
    public List<Vest> findAllVesti();
    public Vest findVestById(Long id);
    public int save(Vest vest);
    public int update(Vest vest);
    public int delete(Long id);
}
