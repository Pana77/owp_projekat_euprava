package com.dao;

import com.models.Korisnik;
import java.util.List;

public interface KorisnikInterface {
    public List<Korisnik> findAllKorisnici();
    public Korisnik findKorisnikById(Long id);
    public Korisnik findKorisnikByEmail(String email);
    public Korisnik findKorisnikWithCredentials(String email, String lozinka);
    public int save(Korisnik korisnik);
    public int update(Korisnik korisnik);
    public int delete(Long id);
}
