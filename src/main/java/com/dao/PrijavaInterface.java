package com.dao;

import com.models.Prijava;

import java.util.List;

public interface PrijavaInterface {
    public List<Prijava> findAllPrijave();
    public Prijava findPrijavaById(Long id);
    public List<Prijava> findAllPrijaveByKorisnik(String pretraga);
    public List<Prijava> searchPrijave(String query);

    public List<Prijava> findPrijavaByPacijentId(Long id);

    public int save(Prijava prijava);
    public int update(Prijava prijava);
    public int delete(Long id);
}
