package com.dao;

import com.models.Pacijent;

import java.util.List;

public interface PacijentInterface {
    public Pacijent findPacijentById(Long id);
    public List<Pacijent> findAllPacijenti();
    public int save(Pacijent pacijent);
    public int update(Pacijent pacijent);
    public int delete(Long id);
}
