package com;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OwpSr242021Application {

	public static void main(String[] args) {
		SpringApplication.run(OwpSr242021Application.class, args);
	}

}
