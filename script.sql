DROP SCHEMA IF EXISTS euprava;
CREATE SCHEMA euprava DEFAULT CHARACTER SET utf8;
USE euprava;

CREATE TABLE proizvodjacVakcine(
    id INT AUTO_INCREMENT PRIMARY KEY,
    ime VARCHAR(30) NOT NULL,
    drzava VARCHAR(30) NOT NULL,
    UNIQUE (id)
);
CREATE TABLE korisnici(
    id INT AUTO_INCREMENT PRIMARY KEY,
    ime VARCHAR(30) NOT NULL,
    prezime VARCHAR(30) NOT NULL,
    email VARCHAR(30) NOT NULL,
    lozinka VARCHAR(20) NOT NULL,
    adresa VARCHAR(40) NOT NULL,
    jmbg VARCHAR(20) NOT NULL,
    datumRodjenja DATE NOT NULL,
    brojTelefona VARCHAR(20) NOT NULL,
    datumivremeregistracije TIMESTAMP NOT NULL,
    uloga VARCHAR(15) NOT NULL,
    UNIQUE (email),
    UNIQUE (brojtelefona)
);
CREATE TABLE vakcine (
    id INT AUTO_INCREMENT PRIMARY KEY,
    ime VARCHAR(30) NOT NULL,
    dostupnakolicina INT NOT NULL,
    proizvodjacvakcine INT NOT NULL,
    UNIQUE (id),
    FOREIGN KEY (proizvodjacvakcine) REFERENCES proizvodjacVakcine(id) ON DELETE CASCADE
);

CREATE TABLE vesti (
    id INT AUTO_INCREMENT PRIMARY KEY,
    naziv VARCHAR(30) NOT NULL,
    sadrzaj VARCHAR(500) NOT NULL,
    datumivremeobjavljivanja DATETIME NOT NULL,
    UNIQUE (id)
);
CREATE TABLE vestoobolelima(
    id INT AUTO_INCREMENT PRIMARY KEY,
    brojobolelih INT NOT NULL,
    brojtestiranih INT NOT NULL,
    brojobolelihodpocetka INT NOT NULL,
    brojhospitalizovanih INT NOT NULL,
    brojpacijenatanarespiratorima INT NOT NULL,
    datumivremeobjavljivanja DATETIME NOT NULL,
    UNIQUE (id)
);
CREATE TABLE pacijenti(
    id INT AUTO_INCREMENT PRIMARY KEY,
    vakcinisan BIT NOT NULL,
    primio TINYINT NOT NULL,
    poslednjadoza DATETIME NOT NULL,
    korisnikid INT NOT NULL,
    UNIQUE (id)
);

CREATE TABLE zahtevi (
                            id INT AUTO_INCREMENT PRIMARY KEY,
                            kolicina INT NOT NULL,
                            razlog VARCHAR(200),
                            datum DATE NOT NULL,
                            stanje VARCHAR(70),
                            radnik INT NOT NULL,
                            vakcina INT NOT NULL,
                            komentar VARCHAR(200),
                            FOREIGN KEY (radnik) REFERENCES korisnici(id) ON DELETE CASCADE,
                            FOREIGN KEY (vakcina) REFERENCES vakcine(id) ON DELETE CASCADE,
                            UNIQUE (id)
);

CREATE TABLE prijave (
                              id INT AUTO_INCREMENT PRIMARY KEY,
                              datum DATETIME NOT NULL,
                              pacijent INT NOT NULL,
                              vakcina INT NOT NULL,
                              FOREIGN KEY (pacijent) REFERENCES korisnici(id) ON DELETE CASCADE,
                              FOREIGN KEY (vakcina) REFERENCES vakcine(id) ON DELETE CASCADE,
                              UNIQUE (id)
);


CREATE FUNCTION ukupnozarazeni(vestiid INT) RETURNS INT
    READS SQL DATA
    DETERMINISTIC
BEGIN
    RETURN (SELECT SUM(brojobolelih) FROM vestoobolelima WHERE datum <= (SELECT datum FROM vestoobolelima WHERE id = vestiid));
    END;